﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_Book
{
    class PhoneBook : Contact
    {
        public bool IfExists(string num, List<Contact> contactlist)
        {
            for (int i = 0; i < contactlist.Count; i++)
            {
                if (num == contactlist[i].Phone)
                {
                    return false;
                }
            }

            return true;
        }

        public List<Contact> Search(string codes, List<Contact> contactlist)
        {
            List<Contact> list = new List<Contact>();
            for (int i = 0; i < contactlist.Count; i++)
            {
                if (!contactlist[i].Phone.StartsWith(codes))
                {
                    list.Add(contactlist[i]);
                }
            }
            return list;
        }
    }
}
