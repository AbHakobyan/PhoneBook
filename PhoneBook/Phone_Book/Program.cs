﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_Book
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Contact> contact = new List<Contact>(10);
            for (int i = 0; i < contact.Count; i++)
            {
                Console.WriteLine(contact[i].Fullname);
            }

            PhoneBook phonecode = new PhoneBook();
            List<Contact> codeFind = phonecode.Search("093", contact);

            for (int i = 0; i < codeFind.Count; i++)
            {
                Console.WriteLine(codeFind[i].Fullname);
            }
            Console.ReadLine();
        }

        public static List<Contact> CreateContact(int count)
        {
            string[] operators = { "093", "077", "094", "091", "055" };
            var rnd = new Random();

            PhoneBook code = new PhoneBook();
            List<Contact> contact = new List<Contact>(); 
            for (int i = 0; i < count; i++)
            {
                int item = rnd.Next(0, operators.Length);
                string number = $"{operators[item]}{rnd.Next(100000, 1000000)}";
                bool chek = code.IfExists(number, contact);
                if (chek)
                {
                    var contacts = new Contact();
                    contacts.Phone = $"{number}";
                    contacts.Name = $"A{i}";
                    contacts.Surename = $"A{i}yan";
                    contacts.Email = $"a{i}@gmail.com";

                    contact.Add(contacts);
                }
                else
                {
                    i--;
                }
            }
            return contact;
        }
    }
}
