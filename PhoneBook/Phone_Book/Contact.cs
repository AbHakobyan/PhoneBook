﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone_Book
{
    class Contact
    {
        public string Name { get; set; }
        public string Surename { get; set; }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set
            {
                if (phone.StartsWith("093") || phone.StartsWith("077") || phone.StartsWith("094") || phone.StartsWith("091") || phone.StartsWith("055"))
                {
                    phone = value;
                }
                else
                {
                    Console.WriteLine("Pliase enter corect number");
                }
            }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set
            {
                if (email.EndsWith("@gmail.com") || email.EndsWith("@yahoo.com") || email.EndsWith("@mail.ru"))
                {
                    email = value;
                }
                else
                {
                    Console.WriteLine("please enter corect email");
                }
            }
        }

        public string Fullname => $"{Name} {Surename} - {Phone} , {Email}";

    }
}
